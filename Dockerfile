FROM alpine:latest

RUN apk add python3
RUN apk add py3-pip
COPY . /app
WORKDIR /app
RUN pip freeze > requirements.txt
RUN apk update && pip install -r /app/requirements.txt --no-cache-dir


CMD ["python3", "app.py"]

EXPOSE 8080